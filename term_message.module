<?php

/**
 * @file
 * Module that provides custom messages per taxonomy term and displays the
 * appropriate message on nodes that have been assigned the term(s).
 *
 * @todo
 *   Skip functionality
 *     Move to DB table
 *   Vocabulary settings
 *     Implement correctly or remove
 *     Change storage to separate table instead of variables
 */

/**
 * Implementation of hook_help().
 */
function term_message_help($path, $arg) {
  switch ($path) {
    case 'admin/help#term_message':
      $output = '<p>'. t('Term message allows users with administer taxonomy permission to provide messages that will:') .'</p>';
      $output .= '<ul>';
      $output .= '<li>'. t('<a href="!term-message-settings" title="Term message settings">Configure site-wide settings for term messages</a>.', array('!term-message-settings' => url('admin/content/term-message'))) .'</li>';
      $output .= '<li>'. t('Edit per-vocabulary settings on the <a href="!vocabulary-listing" title="list vocabularies">vocabularies\' edit pages</a>.', array('!vocabulary-listing' => url('admin/content/taxonomy'))) .'</li>';
      $output .= '<li>'. t('Set messages for terms on individual term add and edit forms.') .'</li>';
      $output .= '</ul>';
      return $output;

    case 'admin/content/term-message':
      $output = '<p>'. t('Configure site-wide settings for term messages below.&nbsp; Edit per-vocabulary settings on the <a href="!vocabulary-listing" title="list vocabularies">vocabularies\' edit pages</a>.&nbsp; Set messages for terms on individual term add and edit forms.', array('!vocabulary-listing' => url('admin/content/taxonomy'))) .'</p>';
      return $output;
  }
}

/**
 * Implementation of hook_perm().
 */
function term_message_perm() {
  return array('administer term messages');
}

/**
 * Implementation of hook_menu().
 */
function term_message_menu() {
  $items['admin/content/term-message'] = array(
    'title' => 'Term messages',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('term_message_admin_settings'),
    'description' => 'Set up how your site handles messages added to terms.  Change sitewide settings for term message functionality.',
    'access arguments' => array('administer taxonomy')
  );

  return $items;
}

/**
 * Implementation of hook_nodeapi().
 */
function term_message_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  if ($page || variable_get('term_message_teaser_lists', FALSE)) {
    switch ($op) {
      case 'view':
        // $node->taxonomy empty array, even if content type cannot have taxonomy
        // but we don't want to assign $term_messages repeatedly
        if (empty($node->taxonomy)) {
          return;
        }
        // Look at other terms and deterimine if we should skip/suppress message
        foreach ($node->taxonomy as $tid => $term) {
          $message = _term_message_load($tid);
          if ($message) {
            $suppress = FALSE;
            // Get skip variable (see TODO at top)
            $skip = variable_get('term_message_skip_'. $tid, array());
            $present = $node->taxonomy;
            // Determine suppression
            foreach (array_keys($skip) as $skip_tid) {
              if (isset($present[$skip_tid])) {
                $suppress = TRUE;
              }
            }
            // If not suppressed, display message
            if (!$suppress) {
              drupal_set_message($message);
            }
          }
        }
        break;
    }
  }
}

/**
 * Implementation of hook_form_alter().
 */
function term_message_form_alter(&$form, $form_state, $form_id) {
  switch ($form_id) {
    case 'taxonomy_form_term':
      // $form_state is empty, should figure out why and try to reimplement using $form_state
      // $tid = $form_state['values']['tid']['#value']; 
      $tid = $form['tid']['#value'];
      $vid = $form['vid']['#value'];
      
      // ?? Looks for permissions
      $show_form = variable_get('term_message_show_form', array($vid => 'all'));
      if ($show_form[$tid] == 'none') {
        return;
      }
      elseif ($show_form[$tid] == 'admin' && !user_access('administer term message')) {
        return;
      }
      
      // Get current message
      $term_message = _term_message_load($tid);
      
      // Form Add
      $form_add = array();
      $form_add['term_message'] = array(
        '#type' => 'fieldset',
        '#title' => t('Term message settings'),
        '#collapsible' => TRUE,
        '#collapsed' => variable_get('term_message_collapse_form', FALSE),
      );
      $form_add['term_message']['term_message_data'] = array(
        '#type' => 'value',
        '#value' => $term_message,
      );
      $form_add['term_message']['term_message'] = array(
        '#type' => 'textarea',
        '#title' => t('Term message'),
        '#default_value' => (isset($term_message)) ? $term_message : '',
        '#description' => t('Provide a message to be displayed on pages of content tagged with this taxonomy term.'),
        '#rows' => 3,
      );
      
      // Get Vocabulary
      $vocabulary = taxonomy_vocabulary_load($vid);
      // Check for multiple type, so that we can provide some overrides.
      if ($vocabulary->multiple) {
        $options = _term_message_terms($vid, $tid);
        $form_add['term_message']['term_message_skip'] = array(
          '#type' => 'select',
          '#title' => t('Suppress message for content with selected terms'),
          '#options' => $options,
          '#default_value' => variable_get('term_message_skip_' . $tid, array()),
          '#multiple' => TRUE,
          '#size' => min(12, count($options)),
          '#description' => t('If one of these terms is also present, do not show the message.'),
        );
      }
      
      // Handled by hook_taxonomy, no custom submit handler needed
      // put the form additions before the buttons
      $pos = array_search('submit', array_keys($form));
      $form = array_merge(array_slice($form, 0, $pos), $form_add, array_slice($form, $pos));
      break;
      
    case 'taxonomy_form_vocabulary':
      // Check for vid
      $vid = isset($form_state['values']['vid']['#value']) ? $form_state['values']['vid']['#value'] : NULL;
      
      // Add form elements
      $form_add = array();
      $form_add['term_message'] = array(
        '#type' => 'fieldset',
        '#title' => t('Term message options'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      // TODO ?? This functionality needs review
      $show_form = variable_get('term_message_show_form', array($vid => 'all'));
      $form_state['term_message']['term_message_admin_access'] = array(
        '#type' => 'radios',
        '#title' => t('Display term message'),
        '#default_value' => $show_form[$vid],
        '#options' => array(
          'all' => t('For all users (with <em>administer taxonomy</em> permission)'),
          'admin' => t('Only for users with <em>administer term message</em> permission'),
          'none' => t('For no users'),
        ),
        '#description' => t('Please note that this only removes the term message textfield from taxonomy term add and edit forms.  Existing term messages are not removed.  This functionality needs review'),
      );
      
      // Handled by hook_taxonomy, no custom submit handler needed
      // put the form additions before the buttons
      $pos = array_search('submit', array_keys($form));
      $form = array_merge(array_slice($form, 0, $pos), $form_add, array_slice($form, $pos));
      break;
  }
}


/**
 * Implementation of hook_taxonomy().
 */
function term_message_taxonomy($op, $type, $array) {
  switch ($type) {
    case 'term':
      switch ($op) {
        case 'insert':
        case 'update':
          // If not set, it's taxonomy manager, and don't overwrite
          if (isset($array['term_message'])) {
            _term_message_save($array);
          }
          // Save skip values
          _term_message_skip_save($array);
          break;
          
        case 'delete':
          _term_message_delete($array['tid']);
          break;
          
      }
      break;
      
    case 'vocabulary':
      switch ($op) {
        case 'insert':
        case 'update':
          _term_message_vocab_save($array);
          break;
          
        case 'delete':
          _term_message_vocab_delete($array);
          break;
          
      }
      break;
      
  }
}

/**
 * Configuration settings for Term message.
 */
function term_message_admin_settings() {
  $form['term_message_collapse_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapse term message fields on edit term form'),
    '#default_value' => variable_get('term_message_collapse_form', FALSE),
  );
  $form['term_message_teaser_lists'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show term messages for listings (not recommended)'),
    '#default_value' => variable_get('term_message_teaser_lists', FALSE),
    '#description' => t('Show term messages on listing pages and whenever the content with the term is processed.  <strong>Likely to result in duplicate and out-of-context messages.</strong>'),
  );
  return system_settings_form($form);
}

/**
 * Retrieve terms
 *
 * @param $vid
 *   vocabulary ID
 * @param $tid
 *   term ID
 * @return
 *   array of term ID and names
 */
function _term_message_terms($vid, $tid) {
  $options = array();
  $terms = _term_message_get_terms_by_vid($vid);
  foreach ($terms as $term) {
    if ($term->tid != $tid) {
      $options[$term->tid] = $term->name;
      // if we do use depth for taxonomy tree result:
      //    $options[$term->tid] = str_repeat('-', $term->depth) . $term->name;
    }
  }
  return $options;
}

/**
 * Save term message and terms to suppress, if any.
 *
 * @param $td
 *   array of term data
 */
function _term_message_save($td) {
  // Get the ID and Message
  $tid = $td['tid'];
  $term_message = $td['term_message'];
  
  // Check if tid exists, insert if it doesn't exist, update if it does exist
  $query_check = "SELECT t.tid FROM {term_message} t WHERE t.tid = %s";
  $exists = db_result(db_query($query_check, $tid));
  if (!$exists) {
    $query_write = "INSERT INTO {term_message} (tid, message) VALUES (%d, '%s')";
    db_query($query_write, $tid, $term_message);
  }
  else {
    $query_write = "UPDATE {term_message} SET tid = %d, message = '%s' WHERE tid = %d";
    db_query($query_write, $tid, $term_message, $tid);
  }
  
  return;
}

/**
 * Load message for term by term id (tid)
 * 
 * @param $tid
 *   string term id
 * @return 
 *   string message
 */
function _term_message_load($tid) {
  $query_write = "SELECT message FROM {term_message} WHERE tid = %d";
  $result = db_result(db_query($query_write, $tid));
  return $result;
}

/**
 * Delete message for term by term id (tid)
 *
 * @param $td
 *   array of term data
 */
function _term_message_delete($tid) {
  // Delete row from table
  $query = "DELETE FROM {term_message} WHERE tid = %d";
  db_query($query, $tid);
  
  return;
}

/**
 * Save term message and terms to suppress, if any.
 *
 * @param $td
 *   array of term data
 */
function _term_message_skip_save($td) {
  // Get the ID and Message
  $tid = $td['tid'];
  $skips = $td['term_message_skip'];

  // Store in variable (TODO change)
  // if there is a tid
  if ($tid) {
    variable_set('term_message_skip_' . $tid, $skips);
  }
  
  return;
}

/**
 * Save vocab settings (See top TODO)
 *
 * @param $vd
 *   Vocabulary data.
 */
function _term_message_vocab_save($vd) {
  $vid = $vd['vid'];
  $show_form = variable_get('term_message_show_form', array());
  $show_form[$vid] = isset($vd['term_message_show_form']) ? $vd['term_message_show_form'] : NULL;
  variable_set('term_message_show_form', $show_form);
  return;
}

/**
 * UNDOC (See top TODO)
 */
function _term_message_vocab_delete($vd) {
  $vid = $vd['vid'];
  $show_form = variable_get('term_message_show_form', array());
  unset($show_form[$vid]);
  variable_set('term_message_show_form', $show_form);
  return;
}


/**
 * Returns an array of all the terms in a vocabulary.
 *
 * @param $vid
 *   Vocabulary ID of the terms that should be returned.
 *
 * @param $page_increment
 *   Optional.  Only used if $depth == FALSE.  Provide pager for large vocabs.
 *
 * Note:  This is modeled on the code used in taxonomy_overview_terms()
 * taxonomy_overview_terms($vid) could be refactored to use this:
 * $terms = term_message/taxonomy_get_terms_by_vid($vid, 25, $vocabulary->tags);
 * if ($vocabulary->tags) {
 *   foreach ($terms as $term) {
 *     $rows[] = array(
 *       l($term->name, "taxonomy/term/$term->tid"),
 *       l(t('edit'), "admin/content/taxonomy/edit/term/$term->tid", array(), $destination),
 *     );
 *   }
 * }
 * else {
 *   foreach ($terms as $term) {
 *     $total_entries++; // we're counting all-totals, not displayed
 *     ...
 * and it continues on from there the same.
 * (Personally I would also factor out the admin link building.)
 */
function _term_message_get_terms_by_vid($vid, $depth = TRUE, $page_increment = 25) {
  if (!$depth) {
    // We are not calling taxonomy_get_tree because that might fail with a big
    // number of tags in the freetagging vocabulary.
    $results = pager_query(db_rewrite_sql("
      SELECT t.*, h.parent FROM {term_data} t INNER JOIN  {term_hierarchy} h ON t.tid = h.tid WHERE t.vid = %d ORDER BY weight, name', 't', 'tid'), " . $page_increment . ", 0, NULL, " . $vid . "
    "));
    
    // Below feels efficient, but only way to put result objects in array?
    $terms = array();
    while ($term = db_fetch_object($results)) {
      $terms[] = $term;
    }
  }
  else {
    $terms = taxonomy_get_tree($vid);
  }
  return $terms;
}